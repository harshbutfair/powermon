#!/bin/bash

set -e
set -o pipefail

# Query database for usage from the last 32 minutes. This will fail if we've missed 3 updates in a row.
# This query gives the number of sets of results - zero (no results) or one (some results).
COUNT=$(influx -ssl -unsafeSsl -database powermon -execute "SELECT * FROM usage WHERE time > $(date -d '32 min ago' +%s)000000000" -format json | jq ".results | .[] | length") 

if [ "${COUNT}" -gt 0 ]
then
    exit 0
else
    exit 1
fi

