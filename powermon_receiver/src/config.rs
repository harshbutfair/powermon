use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub listen_port: u16,
    pub influxdb_host: String,
    pub influxdb_database: String,
    pub influxdb_user: String,
    pub influxdb_password: String
}
