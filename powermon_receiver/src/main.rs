#![warn(clippy::nursery)]

#[macro_use]
extern crate log;

mod config;

use config::Config;
use influxdb::InfluxDbWriteable;
use std::net::{Ipv4Addr, SocketAddrV4, UdpSocket};
use std::path::Path;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;

/*
17 blinks (on + off) => 17 watt hours
10 * ch1_count / (end_time - start_time)      blinks per second
               * 60 * 60                      blinks per hour
               / 1000                         watts to kilowatts

BUT ch1_count counts on + off transitions, so divide again by two

=> 18 * ch1_count / (end_time - start_time)
*/

const CONFIG_FILE: &str = "powermon.toml";
const EXIT_FAILURE: i32 = 1;

const BUFFER_SIZE: usize = 64;
const MESSAGE_SIZE: usize = 60;

// MESSAGE FORMAT
// Start time (since boot) - 4 bytes seconds, 4 bytes microseconds
const START_TIME_OFFSET: usize = 0;
// End time (since boot) - 4 bytes seconds, 4 bytes microseconds
const END_TIME_OFFSET: usize = 8;
// Number of LED blinks. 10 samples, 16 bits each. Each sample should
// be evenly distributed across the time period.
const SAMPLES_PER_MSG: i32 = 10;
const CHANNEL_1_BLINKS_OFFSET: usize = 16;
//const CHANNEL_2_BLINKS_OFFSET: usize = 36;
// Raw 16 bit brightness values from the last read, for debugging/tuning.
const CHANNEL_1_LAST_SAMPLE_OFFSET: usize = 56;
const CHANNEL_2_LAST_SAMPLE_OFFSET: usize = 58;

fn main() {
    // Setup logging
    let formatter = syslog::Formatter3164 {
        facility: syslog::Facility::LOG_USER,
        hostname: None,
        process: "powermon_receiver".into(),
        pid: std::process::id(),
    };

    let logger =
        syslog::udp(formatter, "0.0.0.0:0", "192.168.0.3:514").expect("Failed to start syslog");
    log::set_boxed_logger(Box::new(syslog::BasicLogger::new(logger)))
        .map(|()| log::set_max_level(log::LevelFilter::Info))
        .expect("Failed to set logger");

    let config_path = Path::new(CONFIG_FILE);
    if !config_path.exists() {
        if let Ok(path) = std::env::current_exe() {
            if let Some(path) = path.parent() {
                std::env::set_current_dir(path).unwrap();
            }
        }
    }

    // Read config file into string.
    let config_str = match std::fs::read_to_string(config_path) {
        Ok(s) => s,
        Err(e) => {
            error!("Failed to read config file: {}", e);
            std::process::exit(EXIT_FAILURE);
        }
    };

    // Parse config file.
    let config = match toml::from_str::<Config>(&config_str) {
        Ok(conf) => conf,
        Err(e) => {
            error!("Config file parsing failed: {}", e);
            std::process::exit(EXIT_FAILURE);
        }
    };

    // Start ctrl-c handler.
    let keep_running = Arc::new(AtomicBool::new(true));
    let keep_running_clone = keep_running.clone();
    if let Err(e) = ctrlc::set_handler(move || {
        keep_running_clone.store(false, Ordering::SeqCst);
    }) {
        error!("Error installing ctrl-c handler: {}", e);
    }

    let addr = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), config.listen_port);
    let socket = UdpSocket::bind(addr).map_err(|e| e.to_string()).unwrap();

    socket
        .set_read_timeout(Some(Duration::from_millis(500)))
        .map_err(|e| e.to_string())
        .unwrap();

    // HTTP client with host authentication disabled for self-signed certificate
    let http_client = reqwest::ClientBuilder::new()
        .danger_accept_invalid_hostnames(true)
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap();

    // Database client
    let client = influxdb::Client::new(config.influxdb_host, config.influxdb_database)
        .with_auth(config.influxdb_user, config.influxdb_password)
        .with_http_client(http_client);

    // Runtime - required for calling async function
    let rt = match tokio::runtime::Runtime::new() {
        Ok(rt) => rt,
        Err(e) => {
            error!("Error installing tokio runtime: {}", e);
            std::process::exit(EXIT_FAILURE);
        }
    };

    let mut buffer = [0; BUFFER_SIZE];
    while keep_running.load(Ordering::SeqCst) {
        match socket.recv_from(&mut buffer) {
            Ok((num_bytes, src)) if num_bytes == MESSAGE_SIZE => {
                debug!("Received {} bytes from {}", num_bytes, src);
                let future = handle_message(&buffer, &client);
                if let Err(e) = rt.block_on(future) {
                    error!("Error: {}", e);
                }
            }
            Ok((num_bytes, _src)) if num_bytes == 1 => {
                // Ignore, this is most likely a monit connection check
            }
            Ok((num_bytes, src)) => {
                warn!(
                    "Received {} bytes from {}, expected {}",
                    num_bytes, src, MESSAGE_SIZE
                );
            }
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => (),
            Err(e) => error!("UDP receive error: {}", e.to_string()),
        }
    }
}

async fn handle_message(buffer: &[u8], influxdb: &influxdb::Client) -> Result<(), String> {
    let now = chrono::offset::Utc::now();

    let start_time_s = get_u32_le(buffer, START_TIME_OFFSET);
    let start_time_us = get_u32_le(buffer, START_TIME_OFFSET + 4);
    let end_time_s = get_u32_le(buffer, END_TIME_OFFSET);
    let end_time_us = get_u32_le(buffer, END_TIME_OFFSET + 4);
    let last_ch1 = get_u16_le(buffer, CHANNEL_1_LAST_SAMPLE_OFFSET);
    let last_ch2 = get_u16_le(buffer, CHANNEL_2_LAST_SAMPLE_OFFSET);

    let timestamp_str = format!("{}", now.format("%Y-%m-%d %H:%M:%S"));

    trace!(
        "Report received at {} with times {}.{:06} -> {}.{:06}",
        timestamp_str,
        start_time_s,
        start_time_us,
        end_time_s,
        end_time_us
    );

    // 15/03/2023 cast values to prevent a 'u' suffix from being added.
    // influxdb doesn't like the 'u' suffix. Some dependency upgrade seems
    // to have broken this. Similarly influx is expecting float rather than
    // integer.
    let query = influxdb::Timestamp::from(now)
        .into_query("samples")
        .add_field("ch1_reading", last_ch1 as f32)
        .add_field("ch2_reading", last_ch2 as f32);
    influxdb.query(query).await.map_err(|e| e.to_string())?;

    let start_time = chrono::Duration::seconds(i64::from(start_time_s))
        + chrono::Duration::microseconds(i64::from(start_time_us));
    let end_time = chrono::Duration::seconds(i64::from(end_time_s))
        + chrono::Duration::microseconds(i64::from(end_time_us));
    let duration = end_time - start_time;
    let duration_secs = (duration.num_milliseconds() as f64).mul_add(1e-6, duration.num_seconds() as f64);
    let estimated_start = now - duration;
    let estimated_sample_offset = duration / SAMPLES_PER_MSG;

    for i in 0..(SAMPLES_PER_MSG as usize) {
        let channel1_count = get_u16_le(buffer, CHANNEL_1_BLINKS_OFFSET + 2 * i);
        //let channel2_count = get_u16_le(buffer, CHANNEL_2_BLINKS_OFFSET + 2 * i);
        let estimated_time = estimated_start + estimated_sample_offset * i as i32;

        let query = influxdb::Timestamp::from(estimated_time)
            .into_query("usage")
            .add_field("kWh", 18.0 * f64::from(channel1_count) / duration_secs);
        influxdb.query(query).await.map_err(|e| e.to_string())?;
    }

    Ok(())
}

// Get little-endian u32 from specified offset of buffer.
fn get_u32_le(buffer: &[u8], offset: usize) -> u32 {
    u32::from(buffer[offset + 3]) << 24
        | u32::from(buffer[offset + 2]) << 16
        | u32::from(buffer[offset + 1]) << 8
        | u32::from(buffer[offset])
}

fn get_u16_le(buffer: &[u8], offset: usize) -> u16 {
    u16::from(buffer[offset + 1]) << 8 | u16::from(buffer[offset])
}
