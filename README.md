# Overview

- The overall goal is to measure the number of times that a light on a power meter flashes.
- To support a battery-powered hardware solution, efficiency is most important!
- The ultra low power processor (ULP) wakes up periodically to measure the value from a photo resistor.
- The ULP wakes up the main processor periodically.
- The main processor connects to wifi, sends results via UDP, and immediately disconnects.

# Hardware

- Any ESP32, I used the ESP32 development board from [ezsbc](https://www.ezsbc.com) for this project.
- One or two photo resistors, and one or two 10k resistors to go with them
- Pinouts may differ between different ESP32 variants!
- A power source. USB would be easy if there was AC power available. I use a 3.7v 18650 battery with corresponding battery holder. I previously used 4x AA rechargable batteries
  (4.8v total) but the batteries didn't last as long and it stopped working after a while for no apparent reason.
- Some way to mount the photo resistor(s) such that they sit over the blinking lights of the power meter.

See [wiring.txt](notes/wiring.txt) for ASCII wiring diagram. Note that pin outs may differ on different ESP32 variants. It is important that the selected ADC inputs are accessible by the ULP.

# ULP algorithm

- Implemented in main/ulp/adc.S
- Constants define the ADC channel, how many samples to take on each iteration, the threshold for on vs off, and the number of cycles before waking up the main process.
- On each wakeup (every 25ms):
    - Take the required number of samples.
    - Find the average reading.
    - Compare reading against threshold.
    - Set last\_result to 1 for low values, 2 for high values.
    - If there has been a transition since last time, increment low\_to\_high\_count or high\_to\_low\_count.
- Once the required number of cycles have elapsed, wake the main processor.

# Main program algorithm

- On startup, the `init_ulp_program` function configures GPIO and how often the ULP program should wake up.
- When the ULP program wakes up the main program (every 10 minutes):
    - Connect to wifi.
    - Populate message contents with the number of times the light has blinked for each minute.
    - Send message over UDP.
    - Restart the ULP program and go back to sleep.

# Configuration

- Wifi credentials in main/config.h
- ESP32 IP address in main/main.c `connect_wifi` function
- Destination IP address in main/main.c `app_main` function

# Message format

- The `powermon_receiver` directory contains a program that receives the UDP messages from the ESP32 and writes them to an influxdb database.
- It would also be possible to hack something up with `nc`.

- Measurement start time (since ESP32 boot)
    - 4 bytes seconds
    - 4 bytes microseconds
- Measurement end time (since ESP32 boot)
    - 4 bytes seconds
    - 4 bytes microseconds
- Number of times LED1 blinked
    - 10x 2 byte values
- Number of times LED2 blinked
    - 10x 2 byte values
- Raw brightness value for last read from LED1 and LED2 respectively
    - 2x 2 byte values (for debugging and tuning)

# Building

- Use Espressif IoT Development Framework (esp-idf)
- source export.sh to setup PATH etc
- `idf.py build` to compile
- `idf.py flash` to flash over USB
- `idf.py monitor` to monitor execution

# Debugging

- Reduce `one_minute_counter` in main/ulp/adc.S so that each "minute" is a shorter duration.
- Connect via USB using `idf.py monitor`
