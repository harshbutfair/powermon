#ifndef CONFIG_H
#define CONFIG_H

#define WIFI_SSID "test"
#define WIFI_PASSWORD "qwertyuiop"

#define SERVER_IP_ADDR "192.168.0.3"
#define SERVER_ETH_ADDR {{ 0x00, 0x12, 0x23, 0x34, 0x45, 0x56 }}

#endif
