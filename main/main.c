/* POWERMON
 *
 * Monitor how fast the LEDs are blinking on the power meter.
 *
 * ARP notice:
 *    - The ESP32 doesn't like responding to ARP requests. Something to do with
 *      wifi power saving.
 *    - For reliability and speed, we try to avoid ARP.
 *    - The ARP address for the server is hardcoded in config.h
 *    - The ARP address for the ESP32 is hardcoded on the server in /etc/ethers
 *      and read by running arp -f /etc/ethers on startup.
*/

/* DEBUG - run idf.py menuconfig and change default log verbosity to enable more logging */

#define ETHARP_SUPPORT_STATIC_ENTRIES   1

#include "config.h"      /* contains wifi parameters */

#include <esp_event.h>
#include <esp_log.h>
#include <esp_sleep.h>
#include <esp_wifi.h>
#include <esp32/ulp.h>
#include <driver/rtc_io.h>
#include <esp_adc/adc_oneshot.h>
#include <nvs_flash.h>
#include <ulp_main.h>
#include <apps/ping/ping_sock.h>

// NOTE edit components/lwip/lwip/src/include/lwip/opt.h and set ETHARP_SUPPORT_STATIC_ENTRIES
// to 1 in order to include hardcoded MAC address for target server (and avoid ARP request).
#include <lwip/prot/ethernet.h>
#include <lwip/etharp.h>

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>

#include <lwip/netdb.h>

#include <stdio.h>
#include <string.h>

#define APP_NAME "powermon"

extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_main_bin_start");
extern const uint8_t ulp_main_bin_end[]   asm("_binary_ulp_main_bin_end");

/* Must equal minutes_before_wakeup in ULP assembler constants */
#define NUM_SAMPLES (10)
const int c_MinutesPerWakeup = NUM_SAMPLES;
const int c_WifiMaxRetries = 3;

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;
static EventGroupHandle_t s_ping_event_group;

/* Powermon custom events. */
ESP_EVENT_DECLARE_BASE(POWERMON_EVENTS);
#define PING_SUCCEEDED_EVENT BIT0
#define PING_FAILED_EVENT    BIT1

/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

/* wifi connection retries */
static int s_retry_num = 0;

/* This function is called once after power-on reset, to load ULP program into
 * RTC memory and configure the ADC.
 */
static void init_ulp_program(void);

/* This function is called every time before going into deep sleep.
 * It starts the ULP program and resets measurement counter.
 */
static void start_ulp_program(void);

/* This function is called on every wake-up. It connects to the wifi router
 * using the credentials in config.h.*/
static bool connect_wifi(void);

/* Initialise the PING task. Pings the server and generates a PING_SUCCEEDED_EVENT 
 * on success, or a PING_FAILED_EVENT if no ping responses were received. */
void initialise_ping(in_addr_t);

/* UDP message structure for sending to server. */
typedef struct Message
{
    uint32_t start_time_s;
    uint32_t start_time_us;
    uint32_t end_time_s;
    uint32_t end_time_us;
    uint16_t sensor1Values[NUM_SAMPLES];
    uint16_t sensor2Values[NUM_SAMPLES];
    uint16_t lastChannel1Sample;
    uint16_t lastChannel2Sample;
} Message;


/******************* main *****************/
void app_main(void)
{
    esp_sleep_wakeup_cause_t cause = esp_sleep_get_wakeup_cause();
    if (cause != ESP_SLEEP_WAKEUP_ULP)
    {
        // First iteration. Initialise ULP program, ADC and GPIO.
        init_ulp_program();
    }
    else 
    {
        // General case. Connect to wifi. Copy data collected by ULP into message structure.
        // Send UDP message to server. Restart ULP program and go back to sleep.
        struct timeval now;
        gettimeofday(&now, NULL);

        // Connection to wifi can take a while, so do this after grabbing the current timestamp.
        if(connect_wifi())
        {
            Message msg;
            msg.start_time_s  = (ulp_start_time_sec_high << 16) | (ulp_start_time_sec_low & UINT16_MAX);
            msg.start_time_us = (ulp_start_time_usec_high << 16) | (ulp_start_time_usec_low & UINT16_MAX);
            msg.end_time_s    = now.tv_sec;
            msg.end_time_us   = now.tv_usec;

            for(int i = 0; i < c_MinutesPerWakeup; ++i)
            {
                msg.sensor1Values[i] = (&ulp_ch1_counts)[i] & UINT16_MAX;
            }
            for(int i = 0; i < c_MinutesPerWakeup; ++i)
            {
                msg.sensor2Values[i] = (&ulp_ch2_counts)[i] & UINT16_MAX;
            }
            msg.lastChannel1Sample = ulp_debug_ch1_last_val & UINT16_MAX;
            msg.lastChannel2Sample = ulp_debug_ch2_last_val & UINT16_MAX;

            struct sockaddr_in dest;
            dest.sin_addr.s_addr = inet_addr(SERVER_IP_ADDR);
            dest.sin_family = AF_INET;
            dest.sin_port = htons(0xe1ec);   // "electricity"

            // Avoid the need for ARP requests on every iteration by using a hardcoded
            // ethernet address for the server.
#if ETHARP_SUPPORT_STATIC_ENTRIES
            struct eth_addr ethaddr = SERVER_ETH_ADDR;
            struct ip4_addr ip;
            ip.addr = dest.sin_addr.s_addr;
            ESP_ERROR_CHECK(etharp_add_static_entry(&ip, &ethaddr));
#else
#warning "Compiling without hardcoded ARP optimisation"
#warning "run idf.py menuconfig and \"Enable ARP static entries\""
#endif

            int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
            if(sock >= 0)
            {
                // THIS DOES NOT MAKE SENSE
                // - The intent was to send the UDP message, then ping the server
                //   and wait for a response as a mechanism to give the message
                //   time to send.
                // - But if we send first and then ping, the message doesn't send!
                // - Pinging first and then sending seems reliable. Why???
                //
                // But I tried so many things to make this work reliably I have
                // given up trying to understand it. Even a simple send, sleep,
                // restart no longer works!

                // Disable wifi powersaving to try to get it to work more reliably.
                esp_wifi_set_ps(WIFI_PS_NONE);

                s_ping_event_group = xEventGroupCreate();
                initialise_ping(dest.sin_addr.s_addr);

                // Wait for the ping to succeed or fail.
                EventBits_t bits = xEventGroupWaitBits(s_ping_event_group,
                        PING_SUCCEEDED_EVENT | PING_FAILED_EVENT,
                        pdFALSE,
                        pdFALSE,
                        portMAX_DELAY);

                if(bits & PING_SUCCEEDED_EVENT)
                {
                    ESP_LOGI(APP_NAME, "Ping succeeded event!");

                    ssize_t res = sendto(sock, &msg, sizeof(msg), 0,
                            (const struct sockaddr*)&dest, sizeof(dest));

                    if(res == sizeof(msg))
                    {
                        ESP_LOGI(APP_NAME, "Sent %d bytes", res);
                    }
                    else
                    {
                        ESP_LOGW(APP_NAME, "Sent %d of %d bytes", res, sizeof(msg));
                    }
                    vTaskDelay(80 / portTICK_PERIOD_MS);
                }
                else if(bits & PING_FAILED_EVENT)
                {
                    // In theory we should be able to go to sleep and try again
                    // next time, but it seems like the main process never wakes
                    // up. Therefore reboot - same end result anyway I guess.
                    ESP_LOGE(APP_NAME, "Ping failed - RESTARTING");
                    esp_restart();
                }
                vEventGroupDelete(s_ping_event_group);
            }
            else
            {
                ESP_LOGW(APP_NAME, "Error opening SOCK_DGRAM socket\n");
            }
        }
    }

    start_ulp_program();
    ESP_ERROR_CHECK(esp_sleep_enable_ulp_wakeup());
    esp_deep_sleep_start();
}

static void init_ulp_program(void)
{
    esp_err_t err = ulp_load_binary(0, ulp_main_bin_start,
            (ulp_main_bin_end - ulp_main_bin_start) / sizeof(uint32_t));
    ESP_ERROR_CHECK(err);

    /* Configure ADC channel */
    /* Note: when changing channel here, also change 'adc_channel' constant
       in adc.S */
    adc_oneshot_unit_init_cfg_t init_config = {
        .unit_id = ADC_UNIT_1,
        .ulp_mode = ADC_ULP_MODE_FSM  /* not RISC-V */
    };

    adc_oneshot_unit_init_cfg_t init_config2 = {
        .unit_id = ADC_UNIT_2,
        .ulp_mode = ADC_ULP_MODE_FSM  /* not RISC-V */
    };

    adc_oneshot_unit_handle_t adc1_handle;
    adc_oneshot_unit_handle_t adc2_handle;
    ESP_ERROR_CHECK(adc_oneshot_new_unit(&init_config, &adc1_handle));
    ESP_ERROR_CHECK(adc_oneshot_new_unit(&init_config2, &adc2_handle));

    adc_oneshot_chan_cfg_t config = {
        .bitwidth = ADC_BITWIDTH_12,
        .atten = ADC_ATTEN_DB_12,
    };

    ESP_ERROR_CHECK(adc_oneshot_config_channel(adc2_handle, ADC_CHANNEL_7, &config));

    /* Set ULP wake up period to 25ms */
    ulp_set_wakeup_period(0, 25000);

    /* Disconnect GPIO12 and GPIO15 to remove current drain through
     * pullup/pulldown resistors.
     * GPIO12 may be pulled high to select flash voltage.
     */
    rtc_gpio_isolate(GPIO_NUM_12);
    rtc_gpio_isolate(GPIO_NUM_15);
    esp_deep_sleep_disable_rom_logging(); // suppress boot messages
}

static void start_ulp_program(void)
{
    /* Reset counters */
    ulp_sample_counter = 0;
    ulp_minute_counter = 0;
    for(int i = 0; i < c_MinutesPerWakeup; ++i)
    {
        (&ulp_ch1_counts)[i] = 0;
        (&ulp_ch2_counts)[i] = 0;
    }

    /* Store start time */
    struct timeval now;
    gettimeofday(&now, NULL);
    ulp_start_time_sec_high = (now.tv_sec >> 16) & UINT16_MAX;
    ulp_start_time_sec_low = now.tv_sec & UINT16_MAX;
    ulp_start_time_usec_high = (now.tv_usec >> 16) & UINT16_MAX;
    ulp_start_time_usec_low = now.tv_usec & UINT16_MAX;

    /* Start the program */
    ESP_ERROR_CHECK(ulp_run(&ulp_entry - RTC_SLOW_MEM));
}

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (s_retry_num < c_WifiMaxRetries)
        {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGW(APP_NAME, "Disconnected from wifi, trying to reconnect");
        }
        else
        {
            ESP_LOGE(APP_NAME, "Failed to connect to wifi");
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

static bool connect_wifi(void)
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_LOGI(APP_NAME, "Erasing NVS before re-initialising");
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_t* netif = esp_netif_create_default_wifi_sta();
    ESP_ERROR_CHECK(esp_netif_dhcpc_stop(netif));

    esp_netif_ip_info_t ipinfo;
    esp_netif_set_ip4_addr(&ipinfo.ip, 192, 168, 0, 57);
    esp_netif_set_ip4_addr(&ipinfo.netmask, 255, 255, 255, 0);
    esp_netif_set_ip4_addr(&ipinfo.gw, 0, 0, 0, 0);

    ESP_ERROR_CHECK(esp_netif_set_ip_info(netif, &ipinfo));

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASSWORD,
            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or
     * connection failed for the maximum number of re-tries (WIFI_FAIL_BIT). The
     * bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we
     * can test which event actually happened. */
    if (bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(APP_NAME, "Connected to AP %s", WIFI_SSID);
    }
    else if (bits & WIFI_FAIL_BIT)
    {
        ESP_LOGW(APP_NAME, "Failed to connect to AP %s", WIFI_SSID);
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);

    return bits & WIFI_CONNECTED_BIT;
}

static void on_ping_success(esp_ping_handle_t hdl, void *args)
{
    ESP_LOGI(APP_NAME, "Ping response received");
    ESP_ERROR_CHECK(esp_ping_stop(hdl));
    // We could use handle to get ping response info if required.
    xEventGroupSetBits(s_ping_event_group, PING_SUCCEEDED_EVENT);
}

static void on_ping_end(esp_ping_handle_t hdl, void *args)
{
    ESP_LOGI(APP_NAME, "Pings completed");
    xEventGroupSetBits(s_ping_event_group, PING_FAILED_EVENT);
}

void initialise_ping(in_addr_t ping_target)
{
    ESP_LOGI(APP_NAME, "Initialise Ping");

    ip_addr_t target_addr;
    ip4_addr_set_u32(&target_addr, ping_target);

    esp_ping_config_t ping_config = ESP_PING_DEFAULT_CONFIG();
    ping_config.target_addr = target_addr;
    ping_config.count = 10;
    ping_config.interval_ms = 200;
    ping_config.timeout_ms = 500;

    /* set callback functions */
    esp_ping_callbacks_t cbs;
    cbs.on_ping_success = on_ping_success;
    cbs.on_ping_timeout = NULL;
    cbs.on_ping_end = on_ping_end;
    cbs.cb_args = NULL;  // arguments that will feed to all callback functions, can be NULL

    esp_ping_handle_t ping;
    ESP_ERROR_CHECK(esp_ping_new_session(&ping_config, &cbs, &ping));
    ESP_ERROR_CHECK(esp_ping_start(ping));
}


